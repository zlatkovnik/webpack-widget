import './styles/style.scss';
import Widget from './components/widget.component';

const widget = new Widget(document.body);
widget.draw();