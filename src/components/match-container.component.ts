import Match from "../models/match.model";
import BetService from "../services/bet.service";
import MatchService from "../services/match.service";
import SportService from "../services/sport.service";

export default class MatchContainer {
    private parent: HTMLElement;
    private container: HTMLElement;
    private quoteElements: HTMLElement[] = [];

    private match: Match;

    private betService: BetService;
    private matchService: MatchService;
    private sportService: SportService;

    constructor(parent: HTMLElement, match: Match) {
        this.parent = parent;
        this.match = match;
        this.betService = BetService.getInstance();
        this.matchService = MatchService.getInstance();
        this.sportService = SportService.getInstance();
    }

    draw() {
        this.container = document.createElement('div');
        this.container.className = 'match-container';
        this.parent.appendChild(this.container);

        this.drawTeamsAndDate();
        this.drawQuotes();
    }

    private drawTeamsAndDate() {
        const teamsDateContainer = document.createElement('div');
        teamsDateContainer.className = 'teams-date-container';
        this.container.appendChild(teamsDateContainer);

        const teamsContainer = document.createElement('div');
        teamsContainer.className = 'teams-container';
        teamsDateContainer.appendChild(teamsContainer);

        const home = document.createElement('div');
        home.innerHTML = this.match.home;
        teamsContainer.appendChild(home);

        const away = document.createElement('div');
        away.innerHTML = this.match.away;
        teamsContainer.appendChild(away);

        const dateContainer = document.createElement('div');
        dateContainer.className = 'date-container';
        teamsDateContainer.appendChild(dateContainer);

        let num;
        num = this.match.kickOffTime.getDate();
        const day = (num < 10) ? `0${num}` : num;
        num = this.match.kickOffTime.getMonth() + 1;
        const month = (num < 10) ? `0${num}` : num;

        const date = document.createElement('div');
        date.innerHTML = `${day}.${month}.`;
        dateContainer.appendChild(date);

        const time = document.createElement('div');
        num = this.match.kickOffTime.getHours();
        const hours = (num < 10) ? `0${num}` : num;
        num = this.match.kickOffTime.getMinutes();
        const minutes = (num < 10) ? `0${num}` : num;
        time.innerHTML = `${hours}:${minutes}`;
        dateContainer.appendChild(time);
    }

    private drawQuotes() {
        const quotesContainer = document.createElement('div');
        quotesContainer.className = 'quotes-container';
        this.container.appendChild(quotesContainer);

        for (const [key, value] of Object.entries(this.match.odds)) {
            this.drawQuote(quotesContainer, key, value as number);
        }
    }

    private drawQuote(quotesContainer: HTMLElement, quoteType: string, quote: number) {
        const quoteContainer = document.createElement('button');
        quoteContainer.className = 'quote-container';
        if (this.betService.bets
            .findIndex(bet => bet.matchCode === this.match.matchCode && bet.quoteType === quoteType) > -1) {
            quoteContainer.className += ' selected';
        }
        quotesContainer.appendChild(quoteContainer);
        this.quoteElements.push(quoteContainer);

        const type = document.createElement('div');
        type.className = 'quote-container--type';
        type.innerHTML = this.matchService.getQuoteName(quoteType);
        if (this.sportService.getSportInfo(this.match.sport).overUnder) {
            type.innerHTML += ' ' + this.match.overUnder;
        }
        quoteContainer.appendChild(type);

        const q = document.createElement('div');
        q.className = 'quote-container--odd';
        if (quote) {
            q.innerHTML = quote.toFixed(2).toString();
        } else {
            q.className += ' disabled';
            q.innerHTML = '-';
            quoteContainer.disabled = true;
        }
        quoteContainer.appendChild(q);

        quoteContainer.onclick = () => {
            const allBets = this.betService.bets;
            const bet = { matchCode: this.match.matchCode, quote: quote, quoteType: quoteType, id: this.match.id };
            this.quoteElements.forEach(el => el.className = 'quote-container');
            // Znaci da opklada za taj mec vec postoji
            if (allBets.find(b => b.matchCode === this.match.matchCode)) {
                const index = allBets.findIndex(b => b.matchCode === this.match.matchCode && b.quoteType === quoteType);
                allBets.splice(allBets.findIndex(b => b.matchCode === this.match.matchCode), 1);
                if (index < 0) {
                    quoteContainer.className += ' selected';
                    allBets.push(bet);
                }
            } else {
                allBets.push(bet);
                quoteContainer.className += ' selected';
            }
            this.betService.notifyAll();
        }
    }
}