import BetObserver from "../models/observer-pattern/bet-observer.model";
import BetService from "../services/bet.service";
import ConfigService from "../services/config.service";
import MatchService from "../services/match.service";
import TranslateService from "../services/translate.service";

export default class Header implements BetObserver {
    private parent: HTMLElement;
    private container: HTMLElement;
    private filterContainer: HTMLElement;

    private leftButton: HTMLElement;
    private rightButton: HTMLElement;
    private currentSport: HTMLElement;

    private currentQuote: HTMLElement;

    private betService: BetService;
    private matchService: MatchService;
    private configService: ConfigService;
    private translateService: TranslateService;


    constructor(parent: HTMLElement) {
        this.parent = parent;
        this.betService = BetService.getInstance();
        this.matchService = MatchService.getInstance();
        this.configService = ConfigService.getInstance();
        this.translateService = TranslateService.getInstance();
        const url = new URL(window.location.href);
        const urlPollInterval = Number(url.searchParams.get("poll_interval"));
        const pollInterval = (urlPollInterval < this.configService.getPollInterval())
            ? this.configService.getPollInterval() : urlPollInterval;
        setInterval(() => {
            this.matchService.refreshMatches().then(() => {
                this.container.remove();
                this.draw();
                this.matchService.notifyAll(true);
                this.container.className += ' flash';
                setTimeout(() => { this.container.className = 'header' }, 2000);
            });
        }, pollInterval);
    }

    draw() {
        this.container = document.createElement('div');
        this.container.className = 'header';
        this.parent.appendChild(this.container);


        this.drawFilter();

        const btn = document.createElement('button');
        btn.className = 'play-a-ticket';
        this.currentQuote = document.createElement('div');
        this.updateBet();
        btn.appendChild(this.currentQuote);
        this.betService.registerObserver(this);
        btn.onclick = () => {
            this.betService.redirectToSite();
        }
        this.container.appendChild(btn);
    }

    private drawFilter() {
        if (this.matchService.getFilteredMatches().length === 0) {
            return;
        }
        this.filterContainer = document.createElement('div');
        this.filterContainer.className = 'filter-container';
        this.container.appendChild(this.filterContainer);

        this.leftButton = document.createElement('button');
        const img = document.createElement('img');
        img.src = require("../assets/imgs/arrow-left.svg").default;
        this.leftButton.appendChild(img);
        this.leftButton.className = 'header--arrow-left';
        this.leftButton.onclick = () => this.handleSportChange(-1);
        this.filterContainer.appendChild(this.leftButton);

        this.currentSport = document.createElement('div');
        this.currentSport.className = 'sport-name';
        this.currentSport.innerHTML = this.matchService.getSelectedSportName();
        this.filterContainer.appendChild(this.currentSport);

        this.rightButton = document.createElement('button');
        const img_ = document.createElement('img');
        img_.src = require("../assets/imgs/arrow-right.svg").default;
        this.rightButton.appendChild(img_);
        this.leftButton.className = 'header--arrow-left';
        this.rightButton.onclick = () => this.handleSportChange(1);
        this.filterContainer.appendChild(this.rightButton);
    }

    private handleSportChange(direction: number) {
        if (direction > 0) {
            this.matchService.selectedIndex = (this.matchService.selectedIndex + 1) % this.matchService.sports.length;
        } else {
            this.matchService.selectedIndex--;
            if (this.matchService.selectedIndex < 0) {
                this.matchService.selectedIndex = this.matchService.sports.length - 1;
            }
        }

        this.currentSport.innerHTML = this.matchService.getSelectedSportName();
        this.matchService.notifyAll(false);
    }

    updateBet() {
        if (!this.currentQuote) {
            return;
        }
        let acc = 1;
        this.betService.bets.forEach(bet => {
            acc *= bet.quote;
        })
        if (acc < 1000000) {
            this.currentQuote.innerHTML = (acc === 1) ? this.translateService.get('seeAll') :
                `${this.translateService.get('playTicket')} <b>x${acc.toFixed(2).toString()}</b>`;
        } else {
            this.currentQuote.innerHTML =
                `${this.translateService.get('playTicket')} <b>>1,000,000.00</b>`;
        }
    }
}