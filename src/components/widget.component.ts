import MatchService from "../services/match.service";
import Header from "./header.component";
import MatchesContainer from "./matches-container.component";

export default class Widget {
    private parent: HTMLElement;
    private container: HTMLElement;

    private header: Header;
    private matchesContainer: MatchesContainer;

    constructor(parent: HTMLElement) {
        this.parent = parent;
    }

    draw() {
        this.container = document.createElement('div');
        this.container.className = 'widget';
        this.parent.appendChild(this.container);

        MatchService.getInstance().refreshMatches().then(() => {
            this.drawHeader();
            this.drawMatchContainer();
        });
    }

    drawHeader() {
        this.header = new Header(this.container);
        this.header.draw();
    }

    drawMatchContainer() {
        this.matchesContainer = new MatchesContainer(this.container);
        this.matchesContainer.draw();
    }
}