import Match from "../models/match.model";
import MatchContainer from "./match-container.component";
import MatchService from "../services/match.service";
import MatchObserver from "../models/observer-pattern/match-observer.model";
import ConfigService from "../services/config.service";
import TranslateService from "../services/translate.service";

export default class MatchesContainer implements MatchObserver {
    private parent: HTMLElement;
    private container: HTMLElement;
    private matchesContainer: HTMLElement;

    private matches: Match[];

    private matchService: MatchService;
    private configService: ConfigService;
    private translateService: TranslateService;

    constructor(parent: HTMLElement) {
        this.parent = parent;
        this.matchService = MatchService.getInstance();
        this.matchService.registerObserver(this);
        this.configService = ConfigService.getInstance();
        this.translateService = TranslateService.getInstance();
    }

    draw(liveUpdate = false) {
        if (!liveUpdate)
            this.matchService.page = 0;

        this.container = document.createElement('div');
        this.container.className = 'body-container';
        this.parent.appendChild(this.container);

        this.matchesContainer = document.createElement('div');
        this.matchesContainer.className = 'matches-container';
        this.container.appendChild(this.matchesContainer);

        this.matches = this.matchService.getFilteredMatches();
        this.drawMatches();

        if (this.matches && this.matches.length > this.matchService.matchesOnPage) {
            const nav = document.createElement('div');
            nav.className = 'nav-container';
            this.container.appendChild(nav);

            const navBtn = document.createElement('button');
            const img = document.createElement('img');
            img.src = require("../assets/imgs/arrow-right.svg").default;
            navBtn.appendChild(img);
            navBtn.onclick = () => {
                this.matchService.page++;
                if (this.matchService.page >= this.matches.length / this.matchService.matchesOnPage)
                    this.matchService.page = 0;
                this.matchesContainer.innerHTML = '';
                this.drawMatches();
            }
            nav.appendChild(navBtn);
        }

    }

    private drawMatches() {
        if (!this.matches || this.matches.length === 0) {
            const offer = document.createElement('div');
            offer.className = 'no-matches-txt';
            offer.innerHTML = this.translateService.get('fullOffer');
            this.matchesContainer.appendChild(offer);
            const noMatches = document.createElement('img');
            noMatches.src = require('../assets/imgs/bg.jpg').default;
            noMatches.className = 'no-matches';
            noMatches.onclick = () => {
                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    window.open(this.configService.mobileRedirect(), '_blank').focus();
                } else {
                    window.open(this.configService.desktopRedirect(), '_blank').focus();
                }
            }
            this.matchesContainer.appendChild(noMatches);
        }
        this.matchService.getPaginatedFilteredMatches().forEach(match => {
            const m = new MatchContainer(this.matchesContainer, match);
            m.draw();
        })
    }

    updateMatch(liveUpdate: boolean) {
        this.container.remove();
        this.draw(liveUpdate);
    }
}