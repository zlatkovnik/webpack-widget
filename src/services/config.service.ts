export default class ConfigService {
    private static instance: ConfigService;

    private config: {
        'API_ENDPOINT': string,
        'MOBILE_REDIRECT': string,
        'DESKTOP_REDIRECT': string,
        'POLL_INTERVAL': number,
        'LANG': string
    };

    constructor() {
        this.config = require("../../widget.config.json");
    }

    static getInstance() {
        if (!this.instance) {
            this.instance = new ConfigService();
        }
        return this.instance;
    }

    domain(): string {
        return this.config.API_ENDPOINT;
    }

    mobileRedirect() {
        return this.config.MOBILE_REDIRECT;
    }

    desktopRedirect() {
        return this.config.DESKTOP_REDIRECT;
    }

    getPollInterval() {
        return this.config.POLL_INTERVAL;
    }

    getLang() {
        return this.config.LANG;
    }
}