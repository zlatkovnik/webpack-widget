import Bet from "../models/bet.model";
import Match from "../models/match.model";
import BetSubject from "../models/observer-pattern/bet-subject.model";
import ConfigService from "./config.service";

export default class BetService extends BetSubject {

    static instance: BetService = null;

    bets: Bet[];

    private domainService: ConfigService;

    constructor() {
        super();
        this.bets = [];
        this.domainService = ConfigService.getInstance();
    }

    static getInstance() {
        if (this.instance == null) {
            this.instance = new BetService();
        }
        return this.instance;
    }

    redirectToSite() {
        let url: string;

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            url = this.domainService.mobileRedirect() + '/?';
        } else {
            url = this.domainService.desktopRedirect() + '/?';
        }
        this.bets.forEach((bet, i) => {
            if (i > 0) {
                url += '&'
            }
            url += `${bet.id}=${bet.quoteType}-${bet.quote}`;
        })

        window.open(url, '_blank').focus();
    }

    refreshBets(matches: Match[]) {
        for (let i = this.bets.length - 1; i >= 0; i--) {
            const index = matches.findIndex(match => match.id === this.bets[i].id);
            if (index < 0) {
                this.bets.splice(i, 1);
            } else {
                this.bets[i].quote = matches[index].odds[this.bets[i].quoteType];
            }
        }
    }
}