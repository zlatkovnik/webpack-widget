import ConfigService from "./config.service";

export default class TranslateService {
    private static instance: TranslateService;

    private configService: ConfigService;

    private selectedLang: string;
    private translations: { [key: string]: string };

    constructor() {
        this.configService = ConfigService.getInstance();
        this.translations = require("../../translation.json");
        const url = new URL(window.location.href);
        const lang = url.searchParams.get("lang");
        this.selectedLang = (lang) ? lang : this.configService.getLang();
    }

    static getInstance() {
        if (!this.instance) {
            this.instance = new TranslateService();
        }
        return this.instance;
    }

    get(term: string) {
        //@ts-ignore
        return this.translations[term][this.selectedLang];
    }

    getSelectedLanguage() {
        return this.selectedLang;
    }
}