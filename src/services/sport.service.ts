import SportInfo from "../models/sport-info.model";

export default class SportService {
    private static instance: SportService = null;

    private sportsInfo: any;

    constructor() {
        this.sportsInfo = require("../../sports.json");
    }

    static getInstance() {
        if (this.instance === null) {
            this.instance = new SportService();
        }
        return this.instance;
    }

    getSportInfo(sport: string): SportInfo {
        const info: SportInfo = this.sportsInfo[sport];
        return info;
    }
}