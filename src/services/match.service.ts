import Match from "../models/match.model";
import MatchSubject from "../models/observer-pattern/match-subject.model";
import BetService from "./bet.service";
import ConfigService from "./config.service";
import TranslateService from "./translate.service";
import SportService from "./sport.service";

export default class MatchService extends MatchSubject {
    static instance: MatchService = null;

    private betService: BetService;
    private domainService: ConfigService;
    private translateService: TranslateService;
    private sportService: SportService;

    private matches: Record<string, Match[]> = {};
    matchesOnPage: number;
    page: number;

    selectedIndex: number;
    sports: string[];

    constructor() {
        super();
        this.matchesOnPage = 3;
        this.page = 0;
        this.selectedIndex = 0;
        this.betService = BetService.getInstance();
        this.domainService = ConfigService.getInstance();
        this.translateService = TranslateService.getInstance();
        this.sportService = SportService.getInstance();


    }

    static getInstance() {
        if (this.instance === null) {
            this.instance = new MatchService();
        }
        return this.instance;
    }

    public refreshMatches() {
        return fetch(this.domainService.domain() + '/offer/' + this.translateService.getSelectedLanguage() + '/top/mob')
            .then(res => res.json())
            .then(data => {
                this.matches = {};
                const matches = data.esMatches;
                this.sports = [];
                matches.forEach((match: Match) => {
                    const sportInfo = this.sportService.getSportInfo(match.sport);
                    if (sportInfo) {
                        if (!this.matches[match.sport]) {
                            this.matches[match.sport] = [];
                            this.sports.push(match.sport);
                        }
                        const oddsHelper: Record<string, number> = {};

                        sportInfo.quoteTypes.forEach(quoteType => {
                            oddsHelper[quoteType] = match.odds[quoteType];
                        });
                        match.odds = oddsHelper;
                        match.kickOffTime = new Date(match.kickOffTime);
                        if (sportInfo.overUnder) {
                            //@ts-ignore
                            match.overUnder = match.params['overUnder'];
                        }
                        this.matches[match.sport].push(match);
                    }
                });
                this.sports.sort(this.sortSportsComparator);
                this.betService.refreshBets(matches);
            });
    }

    public getPaginatedFilteredMatches() {
        const offset = this.page * this.matchesOnPage;
        const filteredMatches = this.getFilteredMatches();
        if (!filteredMatches || filteredMatches && filteredMatches.length === 0) {
            return [];
        } else {
            return filteredMatches
                .slice(offset, offset + this.matchesOnPage);
        }
    }

    public getFilteredMatches() {
        if (Object.keys(this.matches).length === 0 && this.matches.constructor === Object) {
            return [];
        }
        return this.matches[this.sports[this.selectedIndex]];
    }

    private sortSportsComparator = (a: string, b: string) => {

        const p1 = this.sportService.getSportInfo(a).sortPriority;
        const p2 = this.sportService.getSportInfo(b).sortPriority;
        if (p1 > p2) {
            return 1;
        }
        return -1;
    }

    getSelectedSportName() {
        const name = this.sportService.getSportInfo(this.sports[this.selectedIndex])
            .name[this.translateService.getSelectedLanguage() as 'sr_ME' | 'en' | 'it' | 'sq'];
        return name;
    }

    getQuoteName(quoteType: string) {
        return this.translateService.get(quoteType);
    }
}