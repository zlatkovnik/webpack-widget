export default interface Bet {
    id: string;
    matchCode: string;
    quoteType: string;
    quote: number;
}