export default interface Match {
    id: string;
    matchCode: string;
    home: string;
    away: string;
    kickOffTime: Date;
    odds: any;
    sport: string;
    overUnder?: number;
}