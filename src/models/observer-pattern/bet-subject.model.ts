import BetObserver from "./bet-observer.model";

export default class BetSubject {
    observers: BetObserver[] = [];

    registerObserver(observer: BetObserver) {
        this.observers.push(observer);
    }

    notifyAll() {
        this.observers.forEach(o => o.updateBet());
    }
}