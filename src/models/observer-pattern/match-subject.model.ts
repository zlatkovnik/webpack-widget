import MatchObserver from "./match-observer.model";

export default class MatchSubject {
    observers: MatchObserver[] = [];

    registerObserver(observer: MatchObserver) {
        this.observers.push(observer);
    }

    notifyAll(liveUpdate: boolean) {
        this.observers.forEach(o => o.updateMatch(liveUpdate));
    }
}