export default interface SportInfo {
    name: {
        sr_ME: string,
        en: string,
        it: string,
        sq: string
    },
    quoteTypes: string[],
    sortPriority: number,
    overUnder?: boolean
}